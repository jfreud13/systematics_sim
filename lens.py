#!/usr/bin/python
################################################################################
# AUTHOR: Jenna Freudenburg 
# DATE: 5/19/2015
#
# Attributes assigned at instantiation:
#	mvir 	:	the virial mass of the lensing halo, in solar masses
#	conc	:	the concentration parameter of the halo
#	z 		:	the redshift of the halo
#
# Attributes calculated internally :
#	rho_crit	:	the critical density of the universe at the given redshift,
#					in Msun/Mpc^3
#	delta_c		:	the characteristic overdensity of the halo
#	rvir		: 	the virial radius of the halo, in Mpc
#	rs			:	the scale radius (rvir/c), in Mpc
################################################################################

import numpy as np
from astropy.cosmology import Planck13 as pl13

class Lens(object):
	
	def __init__(self,mvir,z,conc=None,cosmology="Planck13"):
		# Assigned properties
		self.z = z
		self.mvir = mvir
		if conc:
			self.c = conc
		else:
			self.c = concentration(mvir)
		
		# Critical values
		self.rho_crit = pl13.critical_density(self.z).to('Msun/Mpc^3').value
		self.delta_c = 200/3.*self.c**3/(np.log(1+self.c)-self.c/(1+self.c))
		
		self.rvir = (3.*mvir/(4*np.pi) * 1/(200*self.rho_crit))**(1./3.)
		self.rs = self.rvir/self.c

		
""" Something to use for a reasonable number for concentration, even though 
	we ultimately treat it ultimately as free param...i.e. this is here only
	if we need it to come up with a non-crazy central value for c """
# Calculates halo concentration from mass and redshift; relation from 
# Mandelbaum et al 2008 (z assumed to be 0.22)
	
def concentration(mvir): 
	lens_z = 0.22

	c0 = 4.6
	sigc0 = 0.7
	beta = 0.13
	sigbeta = 0.07

	h = pl13.H(0).value/100.
	mscale = 1e14/h

	return c0*(mvir/mscale)**(-beta)