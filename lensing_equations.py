import numpy as np
from astropy.cosmology import Planck13 as pl13
import astropy.constants as const
import astropy.units as u
from scipy.integrate import quad

""" Critical SMD """

def smd_crit(mylens,source_z):
	G = const.G.to('Mpc^3/(M_sun s^2)').value
	clight = const.c.to('Mpc/s').value

	ds = pl13.angular_diameter_distance(source_z).value
	dd = pl13.angular_diameter_distance(mylens.z).value
	if mylens.z<source_z:
		dds = pl13.angular_diameter_distance_z1z2(mylens.z,source_z).value
	else: 
		return np.inf
	return clight**2/(4*np.pi*G)*ds/(dd*dds) # Equation 9 in Wright & Brainerd


""" Physical functions """

def smd(x,mylens):
	return 2*mylens.rs*mylens.delta_c*mylens.rho_crit*f(x)

def convergence(x,mylens,source_z):
    smdc = smd_crit(mylens,source_z)
    if smdc == np.inf: 
        return 0
	kappa = smd(x,mylens)/smdc
    if kappa == 'nan':
        return 0
    return kappa
	
def shear(x,mylens,source_z):
	return mylens.rs*mylens.delta_c*mylens.rho_crit/smd_crit(mylens,source_z)*g(x)
	

def dkappa_dm(x,mylens,source_z):
	return ( 2.*mylens.delta_c*mylens.rho_crit/(3.*smd_crit(mylens,source_z))
			*mylens.rs/mylens.mvir*(f(x)-x*dfdx(x)) ) 

def dkappa_dlogm(x,mylens,source_z):
	return np.log(10)*mylens.mvir*dkappa_dm(x,mylens,source_z)

def dkappa_dc(x,mylens,source_z):
	return ( 2*mylens.delta_c*mylens.rho_crit/smd_crit(mylens,source_z)*mylens.rs/mylens.c
			*((2-mylens.delta_c/(c*(1+c)^2))*f(x)+x*dfdx(x)) )

def dgamma_dm(x,mylens,source_z):
	return ( mylens.delta_c*mylens.rho_crit/(3.*smd_crit(mylens,source_z))
			*mylens.rs/mylens.mvir*(g(x)-x*dgdx(x)) ) 

def dgamma_dlogm(x,mylens,source_z):
	return np.log(10)*mylens.mvir*dgamma_dm(x,mylens,source_z)


def dkappa_dc(x,mylens,source_z):
	return ( mylens.delta_c*mylens.rho_crit/smd_crit(mylens,source_z)*mylens.rs/mylens.c
			*((2-mylens.delta_c/(c*(1+c)^2))*g(x)+x*dgdx(x)) )


""" Non-dimensionalized functional forms """

def f(x): # non-dim part of kappa
	if x<1:
		return 1/(x**2-1)*(1-2/np.sqrt(1-x**2)*np.arctanh(np.sqrt((1-x)/(1+x))))
	elif x>1:
		return 1/(x**2-1)*(1-2/np.sqrt(x**2-1)*np.arctan(np.sqrt((x-1)/(1+x))))
	else:
		return 1.0/3.0

def dfdx(x):
	if x<1:
		return -1/(1-x**2)**2*(2*x+1/x-6*x/np.sqrt(1-x**2)*np.arctanh(np.sqrt((1-x)/(1+x))))
	elif x>1:
		return -1/(x**2-1)**2*(2*x+1/x-6*x/np.sqrt(x**2-1)*np.arctan(np.sqrt((x-1)/(x+1))))
	else:
		return -2.0/5.0

def g(x): # non-dim part of gamma
	if x==0:
		return 1.
	if x<1:
		return ( 4/np.sqrt(1-x**2)*(2/x**2+1/(x**2-1))*np.arctanh(np.sqrt((1-x)/(1+x)))
				+4./x**2*np.log(x/2.)-2/(x**2-1) )
	elif x>1:
		return ( 4/np.sqrt(x**2-1)*(2/x**2+1/(x**2-1))*np.arctan(np.sqrt((x-1)/(x+1)))
				+4./x**2*np.log(x/2.)-2/(x**2-1) )
	else:
		return 10./3.+4.*np.log(0.5)
	
def dgdx(x):
	if x==0: 
		return 0
	elif x<1:
		return ( -2/(x**3*(1-x**2)**2.5)*(2.*np.arctanh(np.sqrt((1-x)/(1+x)))*(4.-10.*x**2+9*x**4)
				+np.sqrt(1-x**2)*(x**2-4*x**4+4*(x**2-1)**2*np.log(x/2))) )
	elif x>1:
		return ( -2/(x**3*(x**2-1)**2.5)*(2.*np.arctan(np.sqrt((x-1)/(x+1)))*(4.-10.*x**2+9*x**4)
				+np.sqrt(x**2-1)*(x**2-4*x**4+4*(x**2-1)**2*np.log(x/2))) )
	else:
		return -88./15.+np.log(256)


""" Conversions """
	
def theta2x(theta,mylens): 
	r = theta2r(theta,mylens)
	return r/mylens.rs

def theta2r(theta,mylens):
	return theta/60.*(np.pi/180)*pl13.angular_diameter_distance(mylens.z).value

