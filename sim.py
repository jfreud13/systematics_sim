#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from lens import Lens
import lensing_equations as leq

# Things that must be in pars:
def set_parameters():
    pars  = {}
    pars['n_obj'] = 10000
    pars['covar_signal'] = np.array([[ 0.1101456 , -0.06716351],[-0.06716351,  0.0637477 ]])
    pars['mean'] = np.array([ 4.02353185,  0.81357684])
    pars['covar_noise'] = np.array([[0.05,0.0],[0.0,0.05]])
    pars['p_of_z'] = np.array([0.2,0.3,0.4,0.1])
    pars['redshift_bins'] = np.array([0.1,0.2,0.3,0.4])
    pars['theta_max'] = 10.
    pars['lens_p'] = 0.77 # power-law index of galaxy-cluster correlation function.
    pars['lens_bin'] = 1 # index of lens bin
    pars['lens_frac'] = 0.25 # fraction of things in lens redshift that are actually lens members.
    pars['delta_sys_obs'] = np.random.randn(2)
    pars['amp_sys_obs'] = 1.0
    pars['p_sys'] = 0.6
    pars['min_photoz_frac'] = 0.8
    pars['min_photoz_sys_frac'] = 0.95
    pars['lens_frac'] = 0.25
    pars['lens_mass'] = 1e14
    pars['Sz_scatter'] = make_photoz_scatter_matrix(pars)
    pars['Sz_sys'] = make_photoz_sys_matrix(pars)
    return pars

def make_empty_catalog(pars = None):
    n_obj = pars['n_obj']
    dtype = [('mu_true',np.float),('r_true',np.float),('z_true',np.float),
                 ('mu_noisy',np.float),('r_noisy',np.float),('z_noisy',np.float),
                 ('mu_obs',np.float),('r_obs',np.float),('z_obs',np.float),
                 ('kappa_obs',np.float),('kappa_noisy',np.float),
                 ('theta',np.float),('kappa_true',np.float),('z_lens',np.float)]
    catalog = np.empty(n_obj,dtype=dtype)
    return catalog

def add_truth_observables(catalog, pars = None):
    covar = pars['covar_signal']
    mean  = pars['mean']
    p_of_z = pars['p_of_z']
    redshift_bins = pars['redshift_bins']
    
    truth = np.random.multivariate_normal(mean, covar, size=len(catalog) )
    catalog['mu_true'] = truth[:,0]
    catalog['r_true'] = 10.**(truth[:,1])
    catalog['z_true'] = np.random.choice(redshift_bins, size=catalog.size, p = p_of_z)
    return catalog

def add_positions(catalog, pars = None):
    theta_max = pars['theta_max']
    lens_p = pars['lens_p']
    lens_bin = pars['lens_bin']
    nbins = pars['redshift_bins'].size
    for i in xrange(nbins):
        these = catalog['z_true'] == pars['redshift_bins'][i]
        if i != pars['lens_bin']:
            catalog['theta'][these] = theta_max * np.random.rand(np.sum(these))
        else:
            lens_members = np.random.rand(these.size) < pars['lens_frac']
            catalog['theta'][these[~lens_members]] = theta_max*np.random.rand(np.sum(~lens_members))
            catalog['theta'][these[ lens_members]] = theta_max*(1 - np.random.power( lens_p + 1.,size=np.sum(lens_members)))
    return catalog

def make_photoz_scatter_matrix(pars):
    dim = len(pars['p_of_z'])
    szt = np.zeros((dim,dim))
    diag = np.random.uniform(pars['min_photoz_frac'],1,dim)
    np.fill_diagonal(szt,diag)
    for i,row in enumerate(szt):
        fillvec = np.random.random(dim)
        fillvec[i] = 0
        fillvec = fillvec/np.sum(fillvec)*(1-szt[i][i])
        szt[i] = szt[i] + fillvec
    return szt.T
    
def make_photoz_sys_matrix(pars):
    dim = len(pars['p_of_z'])
    szt = np.zeros((dim,dim))
    diag = np.random.uniform(pars['min_photoz_sys_frac'],1,dim)
    np.fill_diagonal(szt,diag)
    for i,row in enumerate(szt):
        fillvec = np.random.random(dim)
        fillvec[i] = 0
        fillvec = fillvec/np.sum(fillvec)*(1-szt[i][i])
        szt[i] = szt[i] + fillvec
    return szt.T
    
def add_noisy_observables(catalog,pars = None):
    cov_noise = pars['covar_noise']
    mean = np.array([0,0])
    noise = np.random.multivariate_normal(mean, cov_noise, size=len(catalog))
    catalog['mu_noisy'] += noise[:,0]
    catalog['r_noisy'] += 10.**(noise[:,1]) 
    return catalog

def add_systematic_observables(catalog, pars = None):
    delta_sys_obs = pars['delta_sys_obs']
    amp_sys_obs = pars['amp_sys_obs']
    p_sys = pars['p_sys']
    sys = amp_sys_obs * catalog['theta'] ** (-p_sys)
    catalog['mu_obs'] = catalog['mu_noisy'] + sys*delta_sys_obs[0]
    catalog['r_obs'] = catalog['r_noisy']*(1. + sys*delta_sys_obs[1])
    return catalog

def add_kappa(catalog,pars=None):
    pdb.set_trace()
    lens = Lens(pars['lens_mass'],pars['redshift_bins'][pars['lens_bin']])
    zs = catalog['z_true']
    xs = np.array([leq.theta2x(t,lens) for t in catalog['theta']])
    sigma = np.array([leq.smd(x,lens) for x in xs])
    sigma_crit = np.array([leq.smd_crit(lens,z) for z in zs])
    kappa = sigma/sigma_crit
    kappa[~np.isfinite(sigma)] = 0
    kappa[~np.isfinite(sigma_crit)] = 0
    catalog['kappa_true'] = kappa
    return catalog
    
def add_kappa_est_noisy(catalog,pars=None):
    dl = np.array([0,1.0])
    cinv = np.linalg.inv(pars['covar_signal']+pars['covar_noise'])
    data = np.vstack((catalog['mu_noisy'],np.log10(catalog['r_noisy']))).T
    kest_noisy = np.array([np.dot(dl,np.dot(cinv,d))/np.dot(dl,np.dot(cinv,dl)) for d in data])
    catalog['kappa_noisy'] = kest_noisy
    return catalog

def add_kappa_est_sys(catalog,pars=None):
    dl = np.array([0,1.0])
    cinv = np.linalg.inv(pars['covar_signal']+pars['covar_noise'])
    data = np.vstack((catalog['mu_obs'],np.log10(catalog['r_obs']))).T
    kest_obs = np.array([np.dot(dl,np.dot(cinv,d))/np.dot(dl,np.dot(cinv,dl)) for d in data])
    catalog['kappa_obs'] = kest_obs
    return catalog

def add_photoz_scatter(catalog, Sz = None, tag1 = 'z_true', tag2 = 'z_noisy',pars=None):
    for i in xrange( pars['redshift_bins'].size ):
        this_pz = Sz[:,i]
        these = catalog[tag1] == pars['redshift_bins'][i]
        catalog[tag2][these] = np.random.choice(pars['redshift_bins'], 
                                                size=np.sum(these), p = this_pz)
    return catalog

def make_catalog(pars = None):
    catalog = make_empty_catalog(pars = pars)
    catalog = add_truth_observables(catalog, pars = pars)
    catalog = add_positions(catalog, pars = pars)
    catalog = add_kappa(catalog,pars=pars)
    catalog = add_noisy_observables(catalog, pars = pars)
    catalog = add_photoz_scatter(catalog, pars=pars, Sz = pars['Sz_scatter'],
                                 tag1 = 'z_true', tag2 = 'z_noisy')
    catalog = add_kappa_est_noisy(catalog,pars=pars)
    catalog = add_systematic_observables(catalog, pars = pars)
    catalog = add_photoz_scatter(catalog, Sz = pars['Sz_sys'],
                                 tag1 = 'z_noisy', tag2 = 'z_obs',pars=pars)
    catalog = add_kappa_est_sys(catalog,pars=pars)
    return catalog

def save_catalog(catalog,pars=None,fname='mock_catalog.dat'):
    header =  "pars['n_obj'] = " + str(pars['n_obj']) + \
              "\npars['covar_signal'] = " + str(pars['covar_signal'])  + \
              "\npars['mean'] = " + str(pars['mean'])  + \
              "\npars['covar_noise'] = " + str(pars['covar_noise'])  + \
              "\npars['p_of_z'] = " + str(pars['p_of_z'])  + \
              "\npars['redshift_bins'] = " + str(pars['redshift_bins'])  + \
              "\npars['theta_max'] = " + str(pars['theta_max'])  + \
              "\npars['lens_p'] = " + str(pars['lens_p'])  + \
              "\npars['lens_bin'] = " + str(pars['lens_bin'])  + \
              "\npars['lens_frac'] = " + str(pars['lens_frac'])  + \
              "\npars['delta_sys_obs'] = " + str(pars['delta_sys_obs'])  + \
              "\npars['amp_sys_obs'] = " + str(pars['amp_sys_obs'])  + \
              "\npars['p_sys'] = " + str(pars['p_sys'])  + \
              "\npars['min_photoz_frac'] = " + str(pars['min_photoz_frac'])  + \
              "\npars['min_photoz_sys_frac'] = " + str(pars['min_photoz_sys_frac'])  + \
              "\npars['lens_frac'] = " + str(pars['lens_frac'])  + \
              "\npars['lens_mass'] = " + str(pars['lens_mass'])  + \
              "\npars['Sz_scatter'] = " + str(pars['Sz_scatter'])  + \
              "\npars['Sz_sys'] = " + str(pars['Sz_sys'])

    np.savetxt(fname,catalog,header=header)

def bin_data(xdata,ydata,nbins,rng=None):
	n, _ = np.histogram(xdata,bins=nbins,range=rng)
	sy, _ = np.histogram(xdata, bins=nbins, weights=ydata, range=rng)
	sy2, bin_edges = np.histogram(xdata, bins=nbins, weights=ydata**2, range=rng)
	
	mean = sy/n
	std = np.sqrt(sy2/n - mean**2)
	bin_ctrs = (bin_edges-(bin_edges[1]-bin_edges[0])/2.0)[1:]
	
	return mean, std, bin_ctrs

def make_kappa_theta_plot(catalog,pars=None):
    fig,ax = plt.subplots(2,2)
    axes = np.ravel(ax)
    rng = (-1.5,1.0)
    nbins = 50
    
    for i,zbin in enumerate(pars['redshift_bins']):
        if i==1:
            pdb.set_trace()
        # true
        ztr = catalog['z_true']==zbin
        ktr = catalog['kappa_true'][ztr]
        ttr = catalog['theta'][ztr]
        meantr,stdtr,bctr = bin_data(np.log10(ttr),ktr,nbins=nbins,rng=rng)
        axes[i].plot(bctr,meantr,'b-',linewidth=2,label='true')
        
        # noisy
        zns = catalog['z_noisy']==zbin
        kns = catalog['kappa_noisy'][zns]
        tns = catalog['theta'][zns] 
        meanns,stdns,bcns = bin_data(np.log10(tns),kns,nbins=nbins,rng=rng)
        axes[i].plot(bcns,meanns,'r-',linewidth=2,label='noisy')        
       
        # obs
        zob = catalog['z_obs']==zbin#
        kob = catalog['kappa_obs'][zob]
        tob = catalog['theta'][zob]
        meanob,stdob,bcob = bin_data(np.log10(tob),kob,nbins=nbins,rng=rng)
        axes[i].plot(bcob,meanob,'g-',linewidth=2,label='observed')        
        
        if i==2 or i==3: axes[i].set_xlabel(r'$\log_{10}(\theta$)')
        if i==0 or i==2: axes[i].set_ylabel(r'$\kappa$')
        axes[i].set_title('z = '+str(zbin))
        
    plt.legend()
    plt.savefig('kappatheta.png',format='png')
    plt.show()
        

def main(argv):
    pars = set_parameters()
    catalog = make_catalog(pars = pars)
    save_catalog(catalog,pars=pars)
    make_kappa_theta_plot(catalog,pars)


if __name__ == "__main__":
    import pdb, traceback, sys
    try:
        main(sys.argv)
    except:
        thingtype, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)
